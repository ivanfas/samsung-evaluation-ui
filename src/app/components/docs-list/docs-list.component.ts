import { ApiService } from './../../shared/api.service';
import { Component, ViewChild, OnInit } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Doc } from './../../shared/doc';

import { Router } from '@angular/router';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
//import { MatChipInputEvent } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { DocFilter } from 'src/app/shared/doc-filter';

@Component({
  selector: 'app-docs-list',
  templateUrl: './docs-list.component.html',
  styleUrls: ['./docs-list.component.css']
})
export class DocsListComponent implements OnInit {

  DocData: any = [];
  dataSource: MatTableDataSource<Doc>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  docForm: FormGroup;
  displayedColumns: string[] = ['documentNumber', 
                                'documentDate', 
                                'currencyCode', 
                                'documentValue',
                                'currencyDesc',
                                'usdValue',
                                'penValue',
                                'brlValue'];

  constructor(private docApi: ApiService, public fb: FormBuilder) {
    this.docApi.GetDocs(null).subscribe(data => {
      this.DocData = data;
      this.dataSource = new MatTableDataSource<Doc>(this.DocData);
      setTimeout(() => {
        this.dataSource.paginator = this.paginator;
      }, 0);
    })    
  }


  ngOnInit(): void {
    this.initDocForm();
  }

  initDocForm() {
    this.docForm = this.fb.group(new DocFilter)
  }

  submitDocForm() {
    if (this.docForm.valid) {
      this.docApi.GetDocs(this.docForm.value).subscribe(data => {
        this.DocData = data;
        this.dataSource = new MatTableDataSource<Doc>(this.DocData);
        setTimeout(() => {
          this.dataSource.paginator = this.paginator;
        }, 0);
      })
    }
  }

}
