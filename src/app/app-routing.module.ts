import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocsListComponent } from './components/docs-list/docs-list.component';

const routes: Routes = [
  { path: 'docs-list', component: DocsListComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
