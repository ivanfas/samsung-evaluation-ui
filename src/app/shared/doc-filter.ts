export class DocFilter {
  documentNumber: string[];
  currencyCode: string[];
  documentDateStart: string[];
  documentDateEnd: string[];

  constructor() {
    this.documentNumber = [''];
    this.currencyCode = [''];
    this.documentDateStart = [''];
    this.documentDateEnd = [''];
  }
 }