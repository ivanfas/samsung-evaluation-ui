export class Doc {
    documentNumber: String;
    documentDate: String;
    currencyCode: String;
    documentValue: String;
    currencyDesc: String;
    usdValue: String;
    penValue: String;
    brlValue: String;
 }